﻿function Add-CMApplicationMend 
{
<#
.Synopsis
   Mend Application in SCCM
.DESCRIPTION
   Mend Application in SCCM by Application name
.EXAMPLE
   Add-CMApplicationMend -Name 'TestApp_V1'
.EXAMPLE
   Add-CMApplicationMend -Name 'TestApp_V1' -verbose
.NOTES
lontru
#>

    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [String]$Name,
        [Parameter(Mandatory=$false)]
        [String]$Server = "sccm.errorlab.local", #"rmdepl0005.onerm.dk"
        [Parameter(Mandatory=$false)]
        [String]$SiteCode = "ERR", # "PRI"
        [Parameter(Mandatory=$false)]
        [String]$FolderRoot = "$($SiteCode):\DeviceCollection\Applications", #"$($SiteCode):\DeviceCollection\OneRM\Applications"
        [Parameter(Mandatory=$false)]
        [String]$DistributionPointGroup = "All DP"
    )

    Begin
    {
        $elapsed = [System.Diagnostics.Stopwatch]::StartNew()

        Push-Location -Path "$($SiteCode):" # Set the current location to be the site code.
        If (!(Get-Module -Name 'ConfigurationManager')){
            Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -force -Global | Out-Null # Import the ConfigurationManager.psd1 module
        }

        If (!(Get-CMApplication -Name "$Name")){
            Write-Error "Application: $Name was not found!!!!"
            break
        }
    }
    Process
    {
        $folders = Get-WmiObject -ComputerName $Server -Namespace "root\sms\site_$($SiteCode)" -Query "select * from SMS_ObjectContainerNode where ObjectType='5000'"       
       
        $OutputObjs = $null
        $OutputObjs = @()
        ForEach ($folder in $folders){
            $OutputObj = New-Object -TypeName PSobject 
            $OutputObj | Add-Member -MemberType NoteProperty -Name 'Name' -Value $folder.Name
            $OutputObj | Add-Member -MemberType NoteProperty -Name 'FolderID' -Value $folder.ContainerNodeID
            $OutputObjs += $OutputObj    
        }
        
        $CheckFolder = $OutputObjs | Where-Object {$_.Name -eq "$Name"}
        If (!($CheckFolder)){
            Write-Host "Creating folder: $Name"
            New-Item -Path "$FolderRoot\$Name" -ItemType directory
        } Else {
            Write-Host "Folder: $Name already exist - Item ID: $($CheckFolder.FolderID)"
        }
        
        $FolderID = $($CheckFolder.FolderID) # Use the WMI tool to get the FolderID
        $CollectionsInSpecficFolder = Get-WmiObject -ComputerName $Server -Namespace root\sms\site_$siteCode -Query "select * from SMS_Collection where CollectionID is in(select InstanceKey from SMS_ObjectContainerItem where ObjectType='5000' and ContainerNodeID='$FolderID') and CollectionType='2'"
        
        If ($CollectionsInSpecficFolder.Name -notlike "$Name*"){
            
            $uninstallcoll = Get-CMDeviceCollection -Name "$($Name)-uninstall (device)"g
            If (!$uninstallcoll){
                Write-Host "Create Collection : $($Name)-uninstall (device)"
                $null = New-CMDeviceCollection -Name "$($Name)-uninstall (device)" -LimitingCollectionName "All Systems"
                $uninstall_device = Get-CMDeviceCollection -Name "$($Name)-uninstall (device)"
                Move-CMObject -InputObject $uninstall_device -FolderPath "$FolderRoot\$Name" | Out-Null
            }
            $installcoll = Get-CMDeviceCollection -Name "$($Name)-install (device)"
            If (!$installcoll){
                Write-Host "Create Collection : $($Name)-install (device)"
                $null = New-CMDeviceCollection -Name "$($Name)-install (device)" -LimitingCollectionName "All Systems"
                $install_device = Get-CMDeviceCollection -Name "$($Name)-install (device)" 
                Move-CMObject -InputObject $install_device -FolderPath "$FolderRoot\$Name" | Out-Null
            }
        }
       
        #Get-CMDistributionPointGroup | Select-Object name

        Try {
        Start-CMContentDistribution -ApplicationName "$Name" -DistributionPointGroupName "$DistributionPointGroup"
        }
        Catch {
        $InstallType = Get-CMDeploymentType -ApplicationName "$Name"
        Update-CMDistributionPoint -ApplicationName "$Name" -DeploymentTypeName "$($InstallType.LocalizedDisplayName)"
        }
        Finally {
            if ($(Get-CMApplication -Name "$Name" | Select-Object -ExpandProperty 'NumberOfDeployments') -lt '2'){
                If (!($(Get-CMDeployment -CollectionName "$($Name)-install (device)"))){
                    Write-Host "Deploy $Name to: $($Name)-install (device)"
                    Start-CMApplicationDeployment -Name "$Name" -DeployAction 'Install' -CollectionName "$($Name)-install (device)" -DeployPurpose 'Required'
                } 
                If (!($(Get-CMDeployment -CollectionName "$($Name)-uninstall (device)"))){
                    Write-Host "Deploy $Name to: $($Name)-uninstall (device)"
                    Start-CMApplicationDeployment -Name "$Name" -DeployAction 'Uninstall' -CollectionName "$($Name)-uninstall (device)" -DeployPurpose 'Required'
                }
            }  
        }

        If (Get-CMDeployment -CollectionName "$($Name)-install (device)"){
            Write-Host "Collection OK: $($Name)-install (device)" -ForegroundColor Green
        } 
        If (Get-CMDeployment -CollectionName "$($Name)-uninstall (device)"){
            Write-Host "Collection OK: $($Name)-uninstall (device)" -ForegroundColor Green
        } 
        if ($(Get-CMApplication -Name "$Name" | Select-Object -ExpandProperty 'NumberOfDeployments') -eq '2'){
            Write-Host "Deployment OK: NumberOfDeployments = 2" -ForegroundColor Green
        }

        #Invoke-CMClientNotification -NotificationType RequestMachinePolicyNow -DeviceCollectionName "$($Name)-install (device)" 
        #Invoke-CMClientNotification -NotificationType RequestMachinePolicyNow -DeviceCollectionName "$($Name)-uninstall (device)"
    }
    End
    {
        $Endtime = $($elapsed.Elapsed.ToString())
        $Endtime = $Endtime.Substring(0,$Endtime.Length-8)
        Write-Output "Execution time $Endtime seconds."
        Pop-Location
    }
}

Export-ModuleMember -Function Add-CMApplicationMend